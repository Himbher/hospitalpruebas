import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//rutas
import { APP_ROUTES } from './app.routes';
//moduloes
import { PagesModule } from './pages/pages.module';
//temporal
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//servicios
// import { SettingsService } from './services/settings/settings.service';
import { ServiceModule } from './services/service.module';

//APP_ROUTES
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { MedicoComponent } from './pages/medicos/medico.component';


// import { IncrementadorComponent } from './components/incrementador/incrementador.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MedicoComponent
    // IncrementadorComponent
  ],
  imports: [
    BrowserModule, 
    APP_ROUTES,
    PagesModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
