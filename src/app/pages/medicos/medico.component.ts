import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MedicoService, HospitalService } from '../../services/service.index';
// import { HospitalService } from '../../services/service.index';
import { Hospital } from '../../models/hospital.model';
import { Medico } from '../../models/medico.model';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styles: []
})
export class MedicoComponent implements OnInit {

  hospitales: Hospital[]=[];
  medico: Medico = new Medico('','','','','');
  hospital: Hospital=new Hospital('ddd');

  constructor(
    public _medicoService: MedicoService,
    public _hospitalService: HospitalService
  ) { }

  ngOnInit() {
    this._hospitalService.cargarHospitales().subscribe(hospitales=>this.hospitales=hospitales);
  }

  guardarMedico( f: NgForm ){
    console.log(f.valid);
    console.log(f.value);

    if(f.invalid){
      return;
    }
    this._medicoService.guardaMedico(this.medico).subscribe(medico=>{
      console.log(medico);
    });
  }

  cambioHospital( id: string ){
    this._hospitalService.obtenerHospital(id).subscribe(hospital=>this.hospital=hospital);
  }

}
